/**
 * ATM machines allow 4 or 6 digit PIN codes
 * PIN codes cannot contain anything but exactly 4 digits or exactly 6 digits.
 * Your task is to create a method that takes a string and returns true if the PIN is valid and false if it's not.
 */
public class Atm_pin {
    public static boolean validatePIN(String s) {
        boolean length = false;
        boolean digit = true;
        boolean valid = false;
        if(s.length()==4||s.length()==6){
            length = true;
        }
        for(int i = 0; i < s.length(); i ++){
            if(!(Character.isDigit(s.charAt(i)))){
                digit = false;
                return false;
            }
        }
        if(length && digit){
            valid = true;
        }
        return valid;
    }



    public static void main(String[] args) {
        System.out.println(validatePIN("1234"));
    }
}