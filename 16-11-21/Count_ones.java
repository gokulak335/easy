/**
 * Count the amount of ones in the binary representation of an integer
 *  since 12 is 1100 in binary, the return value should be 2.
 */
public class Count_ones {
    public static int countOnes(int n) {
        if (n == 0){
            return 0;
        }else
        {
            return (n & 1) + countOnes(n >> 1);
        }
    }


    public static void main(String[] args) {
        System.out.println(countOnes(100));
    }
}