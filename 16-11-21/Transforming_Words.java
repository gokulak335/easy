/**
 * Write a function that transforms all letters from [a, m]
 * to 0 and letters from [n, z] to 1 in a string.
 */
public class Transforming_Words {
    public static String convertBinary(String str) {
        String s = str.toLowerCase();
        String s1 = s.replaceAll("[a-m]", "0");
        String s2 = s1.replaceAll("[n-z]", "1");

        return s2;
    }


    public static void main(String[] args) {
        System.out.println(convertBinary("house"));
    }
}