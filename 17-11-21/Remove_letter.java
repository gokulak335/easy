/**
 * a function that will remove the letters "a", "b" and "c" from the given string
 *  the modified version. If the given string does not contain "a", "b", or "c", return null.
 */
public class Remove_letter {
    public  static String removeABC(String words) {
        if(words.contains("a") || words.contains("b") || words.contains("c")) {
            return words.replaceAll("[abcABC]", "");
        }
        else {
            return null;
        }
    }


    public static void main(String[] args) {
        System.out.println(removeABC("Tesha is my world"));
    }
}
