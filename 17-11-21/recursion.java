/**
 * a function that calculates the GCD (Greatest Common Divisor) of two numbers recursively.
 */
public class recursion {
    public static int gcd(int a, int b) {
        if (b == 0)
            return a;
        return gcd(b,a%b);
    }


    public static void main(String[] args) {
        System.out.println(gcd(1,10));
    }
}
