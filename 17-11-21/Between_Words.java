/**
 *  a function that takes three string arguments (first, last, and word) and
 *  returns true if word is found between first and last in the dictionary, otherwise false.
 */
public class Between_Words {
    public static boolean isBetween(String first, String last, String word)
    {
        return first.compareTo(word) < 0 && last.compareTo(word) > 0;
    }


    public static void main(String[] args) {
        System.out.println(isBetween("apple", "banana", "azure"));
    }
}