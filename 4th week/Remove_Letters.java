/**
 * Remove the First and Last Letter in Given String
 * If the string is 2 or fewer characters long, return the string itself
 */
public class Remove_Letters {
    public static String removeFirstLast(String str) {
        if (str.length() <= 2) {
            return str;
        }else{
            return str.substring(1,str.length()-1);
        }
    }


    public static void main(String[] args) {
        System.out.println(removeFirstLast("hello"));
    }
}