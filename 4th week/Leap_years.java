/**
 * To check the given Year is Leap Year (or) Not
 * A year must either be divisible by 400 or divisible by 4 and not 100.
 */
public class Leap_years {
    public static boolean isLeap(int year) {
        if ((year % 400 == 0) || (year % 4 == 0) && (!(year % 100 == 0))) {
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        System.out.println(isLeap(2020));
    }
}
