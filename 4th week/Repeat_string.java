/**
 *  function that takes a string t and a number n and returns the repeated string n number of times.
 *  Return "Not a string!" if given argument t is not a string 
 */
public class Repeat_string {
    public static String repeatString(Object t, int n) {
        String result = "";
        if (t instanceof String) {
            for (int i = 0; i < n; i++) {
                result += t;
            }
        }
        else {
            result = "Not a string!";
        }
        return result;
    }


    public static void main(String[] args) {
        System.out.println(repeatString("hello",2));
    }
}
