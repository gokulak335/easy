/**
 * a number (from 1 - 60) and returns a corresponding string of hyphens.
 */
public class Dashes {
    public static String Go(int num) {
        String dashes = "";
        for(int i = 1; i <= num; i++){
            dashes = dashes + "-";
        }
        return dashes;
    }


    public static void main(String[] args) {
        System.out.println(Go(1));
    }
}