/**
 * toInt() : A function to convert a string to an integer.
 * toStr() : A function to convert an integer to a string.
 */
public class StringNumber {
   public static int toInt(String str){
        return Integer.parseInt(str);
    }
    public static String tostring(int number){
        return Integer.toString(number);
   }


    public static void main(String[] args) {
        System.out.println(toInt("4"));
        System.out.println(tostring(4));
    }
}
