/**
 * If an input string contains only uppercase or only lowercase letters.
 */
public class Case {
    public static boolean SameCase(String str) {
        return str.toUpperCase().equals(str) || str.toLowerCase().equals(str);
    }

    public static void main(String[] args) {
        System.out.println(SameCase("hello"));
    }
}