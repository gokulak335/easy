/**
 * a function that takes two numbers and a mathematical operator and returns the result.
 * This program is to calculate numbers using string operations
 */
public class Calculate {
    public static int Operations(int num1,int num2,String operations)
    {
        if(operations=="+")
        {
            return num1 + num2;
        }
        else if(operations=="-")
        {
            return num1 - num2;
        }
        else if (operations=="*")
        {
            return num1 * num2;
        }
        else if (operations=="/")
        {
            return num1 / num2;
        }
        else if (operations=="%")
        {
            return num1 % num2;
        }
        return 0;

    }


    public static void main(String[] args) {
        System.out.println(Operations(4,2,"-"));
    }
}

