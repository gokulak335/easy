/**
 * Takes a character and a string as arguments
 * returns the number of times the character is found in the string.
 */
public class Count {
    public static int charCount(char c, String str) {
        int result=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==c)
                result++;
        }
        return result;
    }


    public static void main(String[] args) {
        System.out.println(charCount('a',"apple"));
    }
}