/**
 *  function that returns the string "Burp"
 *  with the amount of "r's" determined by the input parameters of the function.
 */
public class Input_Paramater {
    public static String longBurp(int n) {
        String a = "";
        for (int i = 0; i < n; i++)
            a += "r";
        return "Bu" + a + "p";
    }

    public static void main(String[] args) {
        System.out.println(longBurp(3));
    }
}