/**A star number is a centered figurate number
 * represents a centered hexagram (six-pointed star)
  */
public class Star_number {
    static int Number(int n) {
        return 6*n*(n-1)+1;
    }


    public static void main(String[] args) {
        System.out.println(Number(2));
    }
}