/**
 * the number is a multiple of 3 the output should be "Fizz".
 * If the number given is a multiple of 5, the output should be "Buzz".
 * If the number given is a multiple of both 3 and 5, the output should be "FizzBuzz"
 * The output should always be a string even if it is not a multiple of 3 or 5.
 */
public class Fiz_Buzz {
    public static String fizzBuzz(int n) {
        if (n % 3 == 0)
        {
            System.out.println("fizz");
        }
        else if (n % 5 == 0)
        {
            System.out.println("Buzz");
        }
        else if (n % 3 == 0 && n % 5 == 0)
        {
            System.out.println("fizzBuzz");
        }
        else
        {
            System.out.println("invalid");
        }

        return null;
    }


    public static void main(String[] args) {
        System.out.println(fizzBuzz(5));
    }
}
