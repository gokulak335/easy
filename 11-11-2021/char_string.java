/**
 * a function that takes a string and returns the middle character(s).
 * If the word's length is odd, return the middle character
 * If the word's length is even, return the middle two characters.
 */
public class char_string {
    public static String getMiddle(String str) {
        String s;
        if (str.length() % 2 == 0)
            s = str.substring((str.length() / 2) - 1, (str.length() / 2) + 1);
        else
            s = str.substring((str.length() / 2), (str.length() / 2) + 1);
        return s;
    }



   public static void main(String[]args){
       System.out.println(getMiddle("test"));
        }
}
