/**
 * a function that finds the word "bomb" in the given string
 *  (not case sensitive). Return "DUCK!" if found, otherwise,"Relax, there's no bomb.".
 */
public class find_boom {
    public static String bomb(String str)
    {
        String l = str.toLowerCase();
        if (l.contains("bomb"))
        {
            return "DUCK!";
        }
        return "Relax, there's no bomb.";
    }


    public static void main(String[] args) {
        System.out.printf(bomb("There is a bomb"));
    }
}
