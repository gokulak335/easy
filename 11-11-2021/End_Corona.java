/**
 * Given the number of people that recover per day recovers
 * the number of new cases per day newCases
 *  the number of currently active cases activeCases
 *   a function to calculate the number of days needed to reach zero active cases.
 */
public class End_Corona {
    public static int endCorona(int recovers, int newCases, int activeCases)
    {

        return (int) Math.ceil( (double) activeCases / (double) (recovers - newCases));

    }




    public static void main(String[] args) {
        System.out.println(endCorona(4000,2000,7700));
    }
}