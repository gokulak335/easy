package com.company;

public class Main {
    static int fuel(int distance)
    {

        if(distance<10)
        {
            return 100;
        }
        else
        {
            return distance*10;
        }
    }

    public static void main(String[] args) {
        System.out.println(fuel(1));
    }
}
