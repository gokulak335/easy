/**
 * a function that returns the number of hashes and pluses in a string
 */
public class hashes_pluese {
    public static int[] hashPlusCount(String s) {
        return new int[] {s.replace("+","").length(),s.replace("#","").length()};
    }


    public static void main(String[] args) {
        System.out.println(hashPlusCount("###+"));
    }
}
