/**
 * a method that accepts a string (of a person's first and last name)
 *  returns a string with the first and last name swapped.
 */
public class Shuffle_Names {
    public static String convertName(String name) {
        String firstName = name.substring(0, name.indexOf(" "));
        String lastName = name.substring(name.indexOf(" "));
        String cName =  lastName + firstName;
        return cName;
    }

    public static void main(String args[]) {
        System.out.println(convertName("stark tony"));
    }
}

