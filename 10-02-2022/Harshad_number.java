import java.util.Scanner;

public class Harshad_number {
    /**
     * A number is said to be Harshad if it's exactly divisible by the sum of its digits.
     * Create a function that determines whether a number is a Harshad or not.
     *
     */
    public static void main(String[] args) {
        System.out.println(hars_no());
    }
    static int hars_no()
    {
        System.out.println("enter the number");
        Scanner sc=new Scanner(System.in);
        int number=sc.nextInt();
        int rem=0;
        int sum=0;
        while(number>0)
        {
            rem=number%10;
            sum=sum+rem;
           number =number/10;
        }
        if(number%sum==0)
        {
            System.out.println("true");
        }
        else
        {
            System.out.println("false");
        }
        return 0;
    }
}
