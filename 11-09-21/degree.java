public class degree {
    public static double to_degree(double num) {
        return (num / Math.PI) * 180;
    }


    public static void main(String[] args) {
        System.out.println(to_degree(Math.PI));
    }
}