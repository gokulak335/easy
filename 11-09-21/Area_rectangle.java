public class Area_rectangle {
    public static int Area(int h, int w) {
        int area = 0;
        if ((h <= 0) || (w <= 0)) {
            area = -1;
        }
        else {
            area = h*w;
        }
        return area;
    }


    public static void main(String[] args) {
        System.out.println(Area(4,3));
    }
}