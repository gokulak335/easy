public class Hello_world {
      public static String word(int num) {
          if (num % 3 == 0 && num % 5 == 0) {
              return "Hello World";
          } else if (num % 3 == 0) {
              return "Hello";
          } else if (num % 5 == 0) {
              return "World";
          }
          return "null";
      }

         public static void main (String[]args){
             word(3);
         }
     }
