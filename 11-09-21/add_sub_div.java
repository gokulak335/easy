public class add_sub_div {
    public static String operation(int num1, int num2) {
        if (num1 + num2 == 24) {
            return "added";
        } else if (num1 - num2 == 24) {
            return "subtracted";
        } else if (num1 * num2 == 24) {
            return "multiplied";
        } else if (num1 / num2 == 24) {
            return "divided";
        } else {
            return "none";
        }
    }

    public static void main(String[] args) {
        System.out.println(operation(2, 12));
    }
}