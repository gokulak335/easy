public class Switch {
    public static int possiblity(int n) {
        return (int) Math.pow(2, n);
    }


    public static void main(String[] args) {
        System.out.println(possiblity(1));
    }
}
