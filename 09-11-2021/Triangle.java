/**
 * This Triangular Number Sequence is generated from a pattern of dots that form a triangle
 * The first 5 numbers of the sequence, or dots.
 */
public class Triangle {
    public static int triangle(int n) {
        return n * (1 + n) / 2;

}

    public static void main(String[] args) {
        System.out.println(triangle(6));
    }
}