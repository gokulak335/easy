/**
 * Create a function that takes three integer arguments (a, b, c)
 * returns the amount of integers which are of equal value.
 */
public class Values {
    public static int equal(int a, int b, int c) {
        int count = 0;

        if (a==b && b==c) {
            count = 3;
        } else if ((a==b && b!=c) || (a==c && a!=b)) {
            count = 2;
        }

        return count;
    }

    public static void main(String[] args) {
        System.out.println(equal(3,4,3));
    }

}
