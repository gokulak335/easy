/**
 * a function that takes two arguments: the original price and the discount
 * percentage as integers and returns the final price after the discount
 */
public class Discount_Price {
    public static double discount(int price, int percentage) {
        return price - price * percentage / 100;
    }


    public static void main(String[] args) {
        System.out.println(discount(1500, 50));
    }
}

