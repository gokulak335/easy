/**
 * a function that takes two numbers and a mathematical operator + - / *
 * and perform a calculation with the given numbers.
 * Using if else if Statement
 */
public class Calc {
    public static int Operations(int num1, char operators, int num2) {
        int result;
        if (operators == '+')
        {
            result = num1 + num2;
        }
        else if (operators == '-')
        {
            result = num1 - num2;
        }
        else if (operators == '*')
        {
            result = num1 * num2;
        }
        else if (operators == '/')
        {
            result = num1 / num2;
        }
        else if (operators == '%')
        {
            result = num1 % num2;
        }
        else
        {
            result = 0;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(Operations(2,'+',2));
    }
}
