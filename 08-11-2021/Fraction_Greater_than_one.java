/**
 * Given a fraction as a string
 *  return whether or not it is greater than 1 when evaluated.
 */
public class Fraction_Greater_than_one {
    public static boolean greaterThanOne(String frac) {
        String[] fracs = frac.split("/");
        return Integer.parseInt(fracs[0]) > Integer.parseInt(fracs[1]);
    }


    public static void main(String[] args) {
        System.out.println(greaterThanOne("1/2"));
    }
}

