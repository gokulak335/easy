/** a method that takes a string and returns the word count
 * To Seperate the words in the sentence
 * Using inbuilt Method split
 */
public class CountWord {
    public static int countWords(String s) {
        String count[] = s.split(" ");
       return count.length;
    }


    public static void main(String[] args) {
        System.out.println(countWords("this program represent  number count in string"));
    }
}
