/**Check if String Ending Matches Second String
 * a function that takes two strings and returns true
 * if the first string ends with the second string; otherwise return false.
 */
public class Checking_Strings {
    public static boolean checkEnding(String str1, String str2){
      return str1.matches("(.*)"+str2+"(.*)");
        }


    public static void main(String[] args) {
        System.out.println(checkEnding("abc","d"));
    }
}
