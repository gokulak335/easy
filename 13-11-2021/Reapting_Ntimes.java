/**
 *a function that repeats each character in a string n times.
 */
public class Reapting_Ntimes {
    public static String repeat(String str, int n) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            for (int j = 0; j < n; j++) {
                result += str.charAt(i);
            }
        }
        return result;
    }


    public static void main(String[] args) {
        System.out.println(repeat("mice", 5));
    }
}