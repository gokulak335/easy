/**
 *  a function that takes a string name and a number num (either 0 or 1)
 *   return "Hello" + name if num is 1, otherwise return "Bye" + name.
 */
public class say_hello_say_bye {
    public static String sayHelloBye(String name, int num) {
        name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        if (num == 1){
            return "Hello " + name;
        } else if (num == 0){
            return "Bye " + name;
        }
        return "";
    }


    public static void main(String[] args) {
        System.out.println(sayHelloBye("alon", 1));
    }
}