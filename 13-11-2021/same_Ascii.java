/**
 * Return true if the sum of ASCII values of the first string is same
 * as the sum of ASCII values of the second string
 *  otherwise return false.
 */
public class same_Ascii {
    public static boolean sameAscii(String a, String b) {
        return a.chars().sum() == b.chars().sum();
    }


    public static void main(String[] args) {
        System.out.println(sameAscii("a", "a"));
    }
}