/**
 * function that receives two portions of a path and joins them
 * The portions will be joined with the "/" separator.
 * There could be only one separator and if it is not present it should be added.
 */
public class Join_two_portions {
    public static String joinPath(String portion1, String portion2) {
        return portion1.replace("/", "") + "/" + portion2.replace("/", "");

    }

    public static void main(String[] args) {
        System.out.println(joinPath("portion1", "portion2"));
    }
}