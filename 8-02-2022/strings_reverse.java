public class strings_reverse {
    /**
     * Write a function that reverses a string. Make your function recursive.
     * 
     */
    public static void main(String[] args) {
        System.out.println(reverse("abc"));
    }
    static String reverse(String s)
    {
        String rev="";
     int len=s.length();
     for(int i=len-1;i>=0;i--)
     {
         rev=rev+s.charAt(i);
     }
     return rev;
    }
}
