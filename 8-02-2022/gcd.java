/**
 * Create a function that takes two numbers as arguments
 * and returns the Greatest Common Divisor (GCD) of the two numbers.
 */
public class gcd {
    public static void main(String[] args) {
        System.out.println(Greatest_Common_Divisor(3,5));

    }

    static int Greatest_Common_Divisor(int num1, int num2)
    {
        int great_common_fact = 1;
        for (int i = 1; i <= num1 && i <= num2; i++)
        {
            if ((num1 % i == 0) && (num2 % i == 0))
            {
                great_common_fact = i;
            }

        }
        return great_common_fact;
    }
}
