/**
 *a method that accepts a string and returns true
 *  if it's in the format of a proper phone number and false
 *  Assume any number between 0-9 (in the appropriate spots) will produce a valid phone number.
 */
public class phone_num {
    public static boolean isValidPhoneNumber(String s) {
        if (s.length() == 14) {
            if (s.charAt(0) == '(' && s.charAt(4) == ')' && s.charAt(5) == ' ' &&
                    s.charAt(9) == '-') {
                return true;
            }
        }

        return false;

    }


    public static void main(String[] args) {
        System.out.println(isValidPhoneNumber("(123) 456-7890"));
    }

}
