/**
 * Given two integers a and b,
 * return how many times a can be halved while still being greater than b.
 */
public class Halve {
    public static int halveCount(double a, int b) {
        int count = 0;
        while(a/2 > b) {
            count++;
            a /= 2;
        }
        return count;
    }


    public static void main(String[] args) {
        System.out.println(halveCount(1324, 98));
    }
}