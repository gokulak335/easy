/**
 * a function that takes a credit card number and only displays the last four characters
 * The rest of the card number must be replaced by ************.
 */
public class Hiding_cardnum {
    public static String cardHide(String card) {
        String result = "";
        for (int i = 0; i < card.length(); i++) {
            if(i < card.length() - 4){
                result += "*";
            }
            else{
                result += card.charAt(i);
            }
        }


        return result;
    }


    public static void main(String[] args) {
        System.out.println(cardHide("1234123456785678"));
    }
}