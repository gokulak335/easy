/**
 * The 1st string's first letter = 2nd string's last letter.
 * The 1st string's last letter = 2nd string's first letter.
 * a function that returns true if a pair of strings constitutes a strange pair, and false otherwise.
 */
public class Strange_Pair {
    public static boolean isStrangePair(String s1, String s2) {
        if (s1.length() == 0 && s2.length() == 0)
        {
            return true;
        }
        if (s1.length() == 0 || s2.length() == 0)
        {
            return false;
        }
        return s1.charAt(0) == s2.charAt(s2.length()-1) && s2.charAt(0) == s1.charAt(s1.length()-1);
    }


    public static void main(String[] args) {
        System.out.println(isStrangePair("sparkling", "groups"));
    }
}
