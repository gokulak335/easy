public class reverse {
    /*
    Create a function that takes an integer n and reverses it
     */
    public static void main(String[] args) {
        System.out.println(rev(123));

    }

    static int rev(int n) {
        int rev = 0;
        while (n != 0) {
            rev = (rev * 10) + n % 10;
            n = n / 10;
        }
        return rev;
    }
}