/**
 *  the variable name must always start with a letter and cannot contain spaces,
 *  though numbers and underscores are allowed to be contained in it also
 *   a function which returns true if a given variable name is valid, otherwise return false.
 */
public class Var_Names {
    public static boolean variableValid(String variable) {
        return !(variable.contains(" ") || Character.isDigit(variable.charAt(0)));
    }

    public static void main(String[] args) {
        System.out.println(variableValid("2TimesN"));
    }
}