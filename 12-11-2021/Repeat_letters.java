/**
 *  a method that takes a string and returns a string
 *   which each character is repeated once.
 */
public class Repeat_letters {
    public static String doubleChar(String s) {
        String result = "";
        for(int i=0; i<s.length(); i++)
        {
            result = result + s.charAt(i) + s.charAt(i);
        }
        return result;
    }


    public static void main(String[] args) {
        System.out.println(doubleChar("string"));
    }
}
