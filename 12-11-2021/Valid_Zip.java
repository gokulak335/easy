/**
 * Zip codes consist of 5 consecutive digits
 * Given a string, write a function to determine whether the input is a valid zip code
 * Must only contain numbers (no non-digits allowed).
 * Must not contain any spaces.
 * Must not be greater than 5 digits in length
 */
public class Valid_Zip {
    public static boolean isValid(String zip) {
        return zip.length() < 6 && zip.replaceAll("[0-9]", "").length() == 0;
    }


    public static void main(String[] args) {
        System.out.println(isValid("59001"));
    }
}
