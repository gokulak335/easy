/**
 * a function that takes a string, checks if it has the same number of x's and o's
 * returns either true or false.
 */
public class xoxos {
    public static boolean getXO(String str) {
        int x = 0;
        int o = 0;
        while (str.length() != 0) {
            if (str.toLowerCase().charAt(0) == 'x') {
                x++;
            }
            if (str.toLowerCase().charAt(0) == 'o') {
                o++;
            }
            str = str.substring(1);
        }
        return x == o;
    }


    public static void main(String[] args) {
        System.out.println(getXO("ooxx"));
    }
}
