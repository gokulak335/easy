/**
 * a function that checks if the argument is an integer or a string
 * Return "int" if it's an integer
 *  "str" if it's a string.
 */
public class String_or_Int {
    public static String intOrString(Object var) {
        if (var instanceof String) {
            return "str";
        }
        else
        {
            return "int";
        }
    }

    public static void main(String[] args) {
        System.out.println(8);
    }
}
