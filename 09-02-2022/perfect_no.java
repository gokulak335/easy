import java.util.Scanner;

/**
 * Create a function that tests whether or not an integer is a perfect number.
 * A perfect number is a number that can be written as the sum of its factors,
 * (equal to sum of its proper divisors) excluding the number itself.
 */

public class perfect_no {
    public static void main(String[] args) {
        perfect();
    }
    static int perfect() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter The Number : ");
        int n = in.nextInt();//6
        int sum=0;
        for (int i = 1; i <n; i++) {
            if(n%i==0){
                sum+=i;//1+2+3
            }
        }
        if(sum==n){
            System.out.println(n + " is a Perfect Number");
        }else {
            System.out.println(n + " is not a Perfect Number");
        }
        return sum;
    }
}

