public class missing_no {
    /**
     *Create a method that takes an array of integers
     * between 1 and 10 (excluding one number) and returns the missing number.
     */
    public static void main(String[] args) {
        array();

    }

    static int array() {
        int[] arr = {1, 2, 4};
        int n = arr.length + 1;
        int sum;
        sum = (n * (n + 1)) / 2;
        for (int i = 0; i < arr.length; i++) {
            sum -= arr[i];
        }
        System.out.println("missing no is" + sum);
        return sum;
    }
}
